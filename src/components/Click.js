import { useEffect, useState } from "react";

const Click = () => {
    const [count, setCount] = useState(0);

    const onButtonClickHandler = () => {
        setCount(count + 1);
    }

    useEffect(() => {
        console.log("Use Effect as componentDidMount")
    }, [])

    useEffect(() => {
        console.log("Use Effect as componentDidMount + componentDidUpdate")
    }, [count])

    useEffect(() => {
        console.log("Use Effect render")

        document.title = `You clicked ${count} times`
    })

    return (
        <div style={{margin: "10px"}}>
            <p>Yoy click {count} times</p>
            <hr></hr>
            <button onClick={onButtonClickHandler}>Click me</button>
        </div>
    )
}

export default Click;